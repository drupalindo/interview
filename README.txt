Soal tes masuk team Halcyon untuk Andreas

1. Clone git repo https://gitlab.com/drupalindo/interview.git lalu masuk ke branch andreas
2. Download Codeigniter 3 dari https://www.codeigniter.com/ & push ke repo
3. Integrasi dengan http://benedmunds.com/ion_auth/ untuk mendapatkan modul authentication dan database users, groups, & users_groups. Lalu push ke repo
4. Integrasi dengan AdminLTE (https://almsaeedstudio.com/preview) untuk membuat halaman login dan backend. push ke repo lagi
5. Integrasi dengan http://www.grocerycrud.com/ untuk membuat menu user management (CRUD) untuk user dan group di back end. jangan lupa push lagi
6. tes hasilnya dengan login dengan berbagai macam user dan group.
7. push kembali ke repo code final dan file sql nya.